/**
 * Created by Robin Brons on 26/03/15.
 */

'use strict';

module.exports = function(grunt) {
    var cfg = {
        src  : 'source',
        dest : 'assets'
    };

    var directoriesCfg = {
        composer: 'vendor',
        composerBin: 'vendor/bin',
        reports: 'logs',
        php: 'application'
    };

    // Project configuration.
    grunt.initConfig({
        pkg : grunt.file.readJSON('package.json'),
        cfg : cfg,
        dir : directoriesCfg,

        watch : {
            styles : {
                files : [
                    '<%= cfg.src %>/less/*',
                    '<%= cfg.src %>/less/*/*',
                    '<%= cfg.src %>/less/*/*/*',
                    '<%= cfg.src %>/js/*'
                ],
                tasks : [
                    'less',
                    'csslint',
                    'cssmin',
                    'clean:after',
                    'uglify'
                ],
                options : {
                    atBegin : true
                }
            },

            options : {
                livereload : true
            }
        },

        clean: {
            before : [
                '<%= cfg.dest %>/css/*.css',
                '<%= cfg.dest %>/js/*.js'
            ],
            after : [
                '<%= cfg.dest %>/css/*.src.css',
                '<%= cfg.dest %>/js/*.src.js'
            ]
        },

        less: {
            css: {
                files: {
                    '<%= cfg.dest %>/css/styles.src.css' : [
                        '<%= cfg.src %>/less/front.less'
                    ]
                }
            }
        },

        cssmin : {
            options: {
                keepSpecialComments: 0,
                rebase: false
            },
            css : {
                files: {
                    '<%= cfg.dest %>/css/style.css' : [
                        '<%= cfg.dest %>/css/styles.src.css'
                    ]
                }
            }
        },

        uglify : {
            mangle : {
                toplevel : true
            },
            squeeze : {
                dead_code : false
            },
            codegen : {
                quote_keys : true
            },
            options : {
                preserveComments : false
            },

            js : {
                files : {
                    '<%= cfg.dest %>/js/script.js' : [
                        '<%= cfg.src %>/js/Site.js'
                    ]
                }
            }
        },

        csslint: {
            strict: {
                options: {
                    'import'                      : 2,
                    'ids'                         : false,
                    'adjoining-classes'           : false,
                    'box-model'                   : false,
                    'unique-headings'             : false,
                    'qualified-headings'          : false,
                    'star-property-hack'          : false,
                    'text-indent'                 : false,
                    'box-sizing'                  : false,
                    'overqualified-elements'      : false,
                    'duplicate-background-images' : false,
                    'unqualified-attributes'      : false,
                    'font-sizes'                  : false,
                    'floats'                      : false,
                    'universal-selector'          : false,
                    'compatible-vendor-prefixes'  : false,
                    'outline-none'                : false,
                    'important'                   : false,
                    'regex-selectors'             : false,
                    'duplicate-properties'        : false,
                    'empty-rules'                 : false
                },
                src: [
                    '<%= cfg.dest %>/css/styles.src.css'
                ]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // CSS
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-csslint');

    // JS
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');

    // JS distribution task.
    grunt.registerTask('dist-js', ['uglify']);

    // CSS distribution task.
    grunt.registerTask('dist-css', ['less', 'csslint', 'cssmin']);

    // Fonts distribution task.
    //grunt.registerTask('dist-fonts', ['copy']);

    // Full distribution task.
    //grunt.registerTask('dist', ['clean', 'dist-css', 'dist-fonts', 'dist-js']);
    grunt.registerTask('dist', ['clean:before', 'dist-css', 'dist-js', 'clean:after']);

    // Test task.
    grunt.registerTask('test', ['less', 'csslint']);

    grunt.registerTask('default', [ 'watch' ]);
};