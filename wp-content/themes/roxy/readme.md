# Roxy's WordPress Theme

Roxy's WordPress Theme

Version 1.0

## Grunt

### Node Modules installeren

De packages die worden gedownload staan in `package.json`.
Overal word de laatste versie van gebruikt.

Ga naar de directory waar de `package.json` in staat (vanaf de Terminal
in je PhpStorm):

    $ cd wp-content/themes/roxy/

Vervolgens de modules installeren:

    $ sudo npm install

*__NB:__ `sudo` is nodig omdat sommige modules zeiken over rechten*

Als het goed is, is hij nu bezig met modules installeren.

### Grunt runnen
Je weet wel hoe het moet.

(

    $ grunt

)

## Bootstrap

### LESS
Ik zou niets aan de Bootstrap source veranderen. Je kunt het beste dingen
overschrijven in je eigen css. Zelfs de Bootstrap `variables.less` niet.

### JS
Aan de JS hoef je niets te veranderen dus je kunt het beste een CDN gebruiken

    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

## jQuery
jQuery inladen via CDN is sneller dan local. Wel laden voor je andere JS

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
